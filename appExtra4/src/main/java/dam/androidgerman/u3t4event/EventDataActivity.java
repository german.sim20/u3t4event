package dam.androidgerman.u3t4event;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private TextView tvEventName;
    private RadioGroup rgPriority;
    private Button btAccept;
    private Button btCancel;

    //TODO Extra4: Botones para la fecha y hora junto con variables y se eliminan los timepicker y el datePicker
    private Button btDate;
    private Button btTime;

    private static int month;
    private static int day;
    private static int year;
    private static int hour;
    private static int minute;

    private String priority="Normal";

    //TODO Extra1_1: Para recuperar la info anterior
    private String currentData = "";

    //TODO Extra1_2: Pasamos el mes de months aqui
    private String[] months=null;

    //TODO Extra1_3: Se agrega el EditText de place y variables para recuperar información
    private EditText etPlace;


    private String[] linea = null;
    private String[] palabra = null;
    private String[] hora = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        savedInstanceState=getIntent().getExtras();

        tvEventName.setText(savedInstanceState.getString("EventName"));

        //TODO Extra1_1: Recuperamos la info del tvCurrentData
        currentData=savedInstanceState.getString("CurrentData");

        //TODO Extra1_3: Comprobamos que info se ha recuperado
        if (!currentData.equalsIgnoreCase("none")){
            //TODO Extra1_3: Guardamos por línea
            linea=currentData.split("\n");

            for (int i =0; i<linea.length;i++){
                /*TODO Extra1_3: Guardamos por palabra o carácteres entre los espacios de forma temporal
                *           donde palabra[0] será  Place:, Priority:, etc
                *           y donde palabra[1] será el valor que nos interesa
                * */
                palabra=linea[i].split(" ");

                //TODO Extra1_3: Establecemos el lugar
                if (i==0 && palabra.length>1){
                    if(palabra[1].equals("")){
                        etPlace.setText("");
                    }else{
                        etPlace.setText(palabra[1]);
                    }

                }
                //TODO Extra1_3: Establecemos la prioridad
                if (i==1){
                    priority=palabra[1];
                    switch (priority){
                        case "Low":
                            rgPriority.check(R.id.rbLow);
                            break;
                        case "Normal":
                            rgPriority.check(R.id.rbNormal);
                            break;
                        case "High":
                            rgPriority.check(R.id.rbHigh);
                            break;
                    }
                }
                //TODO Extra1_3: Establecemos la fecha
                if (i==2){
                    for (int m=0;m<months.length;m++){
                        if (months[m].equals(palabra[2])){
                            day=Integer.parseInt(palabra[1]);
                            month=m;
                            year=Integer.parseInt(palabra[3]);

                        }
                    }

                }
                //TODO Extra1_3: Esablecemos la hora
                if(i==3){
                    hora=palabra[1].split(":");
                    hour=Integer.parseInt(hora[0]);
                    minute=Integer.parseInt(hora[1]);
                }
            }
        }

    }

    private void setUI() {

        tvEventName=findViewById(R.id.tvEventName);
        rgPriority=findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);
        btAccept=findViewById(R.id.btAccept);
        btCancel=findViewById(R.id.btCancel);

        //TODO Extra4: Agremamos los botones y el calendario
        btDate=findViewById(R.id.btShowDate);
        btTime=findViewById(R.id.btShowTime);

        btDate.setOnClickListener(this);
        btTime.setOnClickListener(this);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);

        //TODO Extra1_3: Se vincula el EditText
        etPlace=findViewById(R.id.etPlace);

        //TODO Extra1_2: Se obtiene de strings.xml el array de los meses
        months=getResources().getStringArray(R.array.months);


        //TODO Extra4: Esto ya no sirve debido a que ahora se abrirá un dialog
        //TODO Extra2: dependiendo de si esta en portrait o en landscape estará a false o a true
        /*if (!(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)) {
            dpDate.setCalendarViewShown(true);
        } else {
            dpDate.setCalendarViewShown(false);
        }*/

    }
    //TODO Extra4: Fragment para la fecha
    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year2, int month2, int day2) {
            day = day2;
            month = month2;
            year = year2;
        }
    }
    //TODO Extra4: Fragment para la hora
    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);

            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute2) {
            hour = hourOfDay;
            minute = minute2;
        }
    }
    //TODO Extra4: Esto abrirá el fragment
    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    @Override
    public void onClick(View v) {

        Intent activityResult=new Intent();
        Bundle eventData=new Bundle();
        boolean controller=false; //TODO Extra4: Esto lo gasto para evitar que la activity se cierre al pulsar el boton de la fecha o el de la hora

        switch (v.getId()){

            case R.id.btAccept:

                //TODO Extra1_3: Ajustamos la fecha y se agrega el lugar, la hora y los minutos(String.format se usa porque si no, muestra incorrectamente los minutos(11:2 por ejemplo))
                eventData.putString("EventData","Place: "+etPlace.getText()+"\n"+
                                    "Priority: "+priority+"\n"+
                                    "Date: "+day+" "+months[month]+" "+year+"\n"+
                                    "Hour: "+hour+":"+minute);
                controller=true;
                break;
            case R.id.btCancel:
                //TODO Extra1_1: se envía en el la info anterior
                eventData.putString("EventData",currentData);
                controller=true;
                break;
            //TODO Extra4: Función de los botones nuevos, abrirán el fragment
            case R.id.btShowDate:
                showDatePickerDialog(v);
                break;
            case R.id.btShowTime:
                showTimePickerDialog(v);
                break;
        }
        if (controller) {
            activityResult.putExtras(eventData);
            setResult(RESULT_OK, activityResult);
            finish();
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId){
            case R.id.rbLow:
                priority="Low";
                break;

            case  R.id.rbNormal:
                priority="Normal";
                break;

            case  R.id.rbHigh:
                priority="High";
                break;
        }

    }



}
