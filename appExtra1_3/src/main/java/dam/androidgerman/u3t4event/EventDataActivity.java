package dam.androidgerman.u3t4event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener{

    private TextView tvEventName;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;

    private String priority="Normal";

    //TODO Extra1_1: Para recuperar la info anterior
    private String currentData = "";

    //TODO Extra1_2: Pasamos el mes de months aqui
    private String[] months=null;

    //TODO Extra1_3: Se agrega el EditText de place y variables para recuperar información
    private EditText etPlace;


    private String[] linea = null;
    private String[] palabra = null;
    private String[] hora = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        savedInstanceState=getIntent().getExtras();

        tvEventName.setText(savedInstanceState.getString("EventName"));

        //TODO Extra1_1: Recuperamos la info del tvCurrentData
        currentData=savedInstanceState.getString("CurrentData");

        //TODO Extra1_3: Comprobamos que info se ha recuperado
        if (!currentData.equalsIgnoreCase("none")){
            //TODO Extra1_3: Guardamos por línea
            linea=currentData.split("\n");

            for (int i =0; i<linea.length;i++){
                /*TODO Extra1_3: Guardamos por palabra o carácteres entre los espacios de forma temporal
                *           donde palabra[0] será  Place:, Priority:, etc
                *           y donde palabra[1] será el valor que nos interesa
                * */
                palabra=linea[i].split(" ");

                //TODO Extra1_3: Establecemos el lugar
                if (i==0 && palabra.length>1){
                    if(palabra[1].equals("")){
                        etPlace.setText("");
                    }else{
                        etPlace.setText(palabra[1]);
                    }
                }
                //TODO Extra1_3: Establecemos la prioridad
                if (i==1){
                    priority=palabra[1];
                    switch (priority){
                        case "Low":
                            rgPriority.check(R.id.rbLow);
                            break;
                        case "Normal":
                            rgPriority.check(R.id.rbNormal);
                            break;
                        case "High":
                            rgPriority.check(R.id.rbHigh);
                            break;
                    }
                }
                //TODO Extra1_3: Establecemos la fecha
                if (i==2){
                    for (int m=0;m<months.length;m++){
                        if (months[m].equals(palabra[2])){
                            dpDate.updateDate(Integer.parseInt(palabra[3]),m, Integer.parseInt(palabra[1]));
                        }
                    }
                }
                //TODO Extra1_3: Esablecemos la hora
                if(i==3){
                    hora=palabra[1].split(":");
                    tpTime.setHour(Integer.parseInt(hora[0]));
                    tpTime.setMinute(Integer.parseInt(hora[1]));
                }
            }
        }


    }

    private void setUI() {

        tvEventName=findViewById(R.id.tvEventName);
        rgPriority=findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);
        dpDate=findViewById(R.id.dpDate);
        tpTime=findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAccept=findViewById(R.id.btAccept);
        btCancel=findViewById(R.id.btCancel);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);

        //TODO Extra1_3: Se vincula el EditText
        etPlace=findViewById(R.id.etPlace);

        //TODO Extra1_2: Se obtiene de strings.xml el array de los meses
        months=getResources().getStringArray(R.array.months);



    }

    @Override
    public void onClick(View v) {

        Intent activityResult=new Intent();
        Bundle eventData=new Bundle();

        switch (v.getId()){

            case R.id.btAccept:

                //TODO Extra1_3: Ajustamos la fecha y se agrega el lugar, la hora y los minutos(String.format se usa porque si no, muestra incorrectamente los minutos(11:2 por ejemplo))
                eventData.putString("EventData","Place: "+etPlace.getText()+"\n"+
                                    "Priority: "+priority+"\n"+
                                    "Date: "+dpDate.getDayOfMonth()+" "+months[dpDate.getMonth()]+" "+dpDate.getYear()+"\n"+
                                    "Hour: "+tpTime.getHour()+":"+String.format("%02d",tpTime.getMinute()));

                break;
            case R.id.btCancel:
                //TODO Extra1_1: se envía en el la info anterior
                eventData.putString("EventData",currentData);

                break;

        }
        activityResult.putExtras(eventData);
        setResult(RESULT_OK,activityResult);
        finish();

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId){
            case R.id.rbLow:
                priority="Low";
                break;

            case  R.id.rbNormal:
                priority="Normal";
                break;

            case  R.id.rbHigh:
                priority="High";
                break;
        }

    }
}
